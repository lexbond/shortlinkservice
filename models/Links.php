<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "links".
 *
 * @property integer $id
 * @property string $short
 * @property string $origin
 * @property string $date
 */
class Links extends \yii\db\ActiveRecord
{
    const BASIC_URL = 'http://shortlink.loc'; // Домен для короткого Url
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'links';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['origin'], 'required'],
            [['date'], 'safe'],
            ['origin', 'url', 'defaultScheme' => 'http'],
            [['short', 'origin'], 'string', 'max' => 255],
            [['short'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'short' => 'Короткая ссылка',
            'origin' => 'Оригинальная ссылка',
            'date' => 'Дата создания',
        ];
    }

    public function generate()
    {
        // Проверяем есть ли уже данная ссылка в БД
        $link = self::find()->where('origin=:origin', [':origin'=>$this->origin])->one();
        if($link)
        {
            $this->short = $link->short;
            return $this->getAbsUrl();
        }

        // Генерируем уникальный Url
        $this->getUniqueString();
        $this->date = date("Y-m-d H:i:s");
        if($this->save())
        {
            return $this->getAbsUrl();
        } return false;
    }

    private function getUniqueString()
    {
        // Генерируем уникальную строку средствами Yii, можно и md5
        // Храним в БД длину ссылки, чтобы при окончании доступных ссылок
        // просто генерировать на 1 символ больше
        $length = $this->getLengthUrl();
        $str = Yii::$app->security->generateRandomString($length->length_url);
        // Проверяем наличие такой же в БД
        $exist = self::find()->where(['short'=>$str])->one();
        // Если такой же нет, возвращаем строку
        if(!$exist) { $this->short = $str; return; }
        // Если все доступные ссылки закончились, прибавляем к длине 1 символ и
        // генерируем заново
        else {
            $length->length_url +=1;
            $length->save();
            $this->getLengthUrl();
        }
    }

    private function getAbsUrl()
    {
        return self::BASIC_URL."/".$this->short;
    }

    private function getLengthUrl()
    {
        $length = Config::findOne(['id'=>1]);
        return $length;
    }
}
