$(document).ready(function () {
    $(".link-form").submit(function () {
        var origin = $("#origin").val();
        var pattern = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;

        if((origin!=='') && (pattern.test(origin)))
        {
            $.ajax({
                url: '/site/gen-link',
                type: 'POST',
                data: {
                    origin: origin
                },
                success: function(res){
                    if(res!=='error')
                    {
                        $("#result").html('<div class="alert alert-success"><a href="'+res+'" target="_blank">'+res+'</div>');
                    }
                },
                error: function (res) {
                    $("#result").html('<div class="alert alert-warning">Ошибка выполнения. ПОпробуйте повторить запрос позже..</div>');
                    console.log(res);
                }
            })
        } else { $("#result").html('<div class="alert alert-warning">Введите абсолютный Url (http://mysite.com)</div>'); }

        return false;
    });
});