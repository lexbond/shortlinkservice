<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
use yii\helpers\Url;
$this->title = 'Все ссылки';
?>
<div class="links-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <div class="panel panel-default">
        <div class="panel-body">
           <form action="<?=Url::to(['links/index'])?>" method="POST">
            <div class="row">
               <div class="col-md-5">
               </div>
            <div class="col-md-2">
                <select class="form-control" name="field">
                    <option value="id">ID</option>
                    <option value="origin">Оригинальная ссылка</option>
                    <option value="short">Короткая ссылка</option>
                </select>
            </div>
            <div class="col-md-2">
                <input type="text" class="form-control" name="text">
            </div>
            <div class="col-md-3">
                <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
                <input type="submit" class="btn btn-primary" value="Найти">
                <a href="<?=Url::to(['index'])?>" class="btn btn-danger">Сбросить</a>
            </div>
           </div>
           </form>
        </div>
    </div>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'short',
            'origin',
            [
                'label'=>'Дата создания',
                'value'=> function($model){
                    return Yii::$app->formatter->asDate($model->date, "php: d.m.Y H:i:s");
                },
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>

