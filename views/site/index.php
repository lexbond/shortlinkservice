<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\widgets\ActiveForm;
$this->title = 'Создание ссылки - ShortLinkService';
?>
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="/js/main.js"></script>

<div class="site-index">
    <h2>Генерация новой ссылки</h2>
    <br><br>
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-body">

                <?php $form = ActiveForm::begin(['options'=>['class'=>'link-form'],
                    'validateOnBlur' => false,
                    'validateOnType'         => false,
                    'validateOnChange'       => true,
                    'validateOnSubmit'       => false]);
                ?>

                <?= $form->field($model, 'origin')->textInput(['maxlength' => true, 'placeholder'=>'http://mysite.com','id'=>'origin']) ?>

                <div class="form-group">
                    <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary']) ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
            <div class="panel-footer">
                <div id="result">

                </div>

                <?php
                // Если скрипты отключены, всякое бывает..
                if(Yii::$app->session->hasFlash('res')):
                    $link = Yii::$app->session->getFlash('res'); ?>

                    <div class="alert alert-success">
                        <a href="<?=$link?>" target="_blank"><?=$link?></a>
                    </div>

                <?php endif;?>

                <?php if(Yii::$app->session->hasFlash('err')): ?>
                    <div class="alert alert-warning">
                        <?=Yii::$app->session->getFlash('err')?>
                    </div>
                <?php endif;?>
            </div>
        </div>
    </div>

</div>
