<?php

use yii\db\Migration;

/**
 * Class m171124_050618_create_tables
 */
class m171124_050618_create_tables extends Migration
{
    /**
     * @inheritdoc
     */

    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        Yii::$app->db->createCommand("

            CREATE TABLE `config` (
              `id` int(11) NOT NULL,
              `length_url` int(11) NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
            
            INSERT INTO `config` (`id`, `length_url`) VALUES (1, 5);
            
            CREATE TABLE `links` (
              `id` int(11) NOT NULL,
              `short` varchar(255) NOT NULL,
              `origin` varchar(255) NOT NULL,
              `date` datetime NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
            
            ALTER TABLE `config`
              ADD PRIMARY KEY (`id`);
            
            ALTER TABLE `links`
              ADD PRIMARY KEY (`id`),
              ADD UNIQUE KEY `short` (`short`);
            
            ALTER TABLE `config`
              MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=0;
            
            ALTER TABLE `links`
              MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=0;
        ")->execute();
    }

    public function down()
    {
        Yii::$app->db->createCommand("DROP TABLE config; DROP TABLE links;");

        return false;
    }
//    public function safeUp()
//    {
//
//    }
//
//    /**
//     * @inheritdoc
//     */
//    public function safeDown()
//    {
//        echo "m171124_050618_create_tables cannot be reverted.\n";
//
//        return false;
//    }
}
