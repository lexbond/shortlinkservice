<?php

namespace app\controllers;

use app\models\Links;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;

        return parent :: beforeAction($action);
    }


    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex($page = '')
    {
        $model = new Links();

        // Если приходит просто POST
        if ($model->load(Yii::$app->request->post())) {

            $short = $model->generate();
            if($short)
            {
                Yii::$app->session->setFlash('res', $short);
            } else {
                Yii::$app->session->setFlash('err', 'Ошибка, попробуйте позже...');
            }
        }

        return $this->render('index', ['model' => $model]);
    }


    // Если получаем запрос по Ajax
    public function actionGenLink()
    {
        if(Yii::$app->request->isAjax)
        {
            $origin = Yii::$app->request->post('origin');
            $model = new Links();
            $model->origin = $origin;
            $short = $model->generate();
            if($short)
            {
                echo $short;
            } else {
                echo "error";
            }

        }
    }

    // Перенаправляем на оригинальный URL
    public function actionForward($link)
    {
        $origin = Links::find()->where('short=:short', [':short'=>$link])->one();
        if($origin)
        {
            $this->redirect($origin->origin);
        }
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
}
